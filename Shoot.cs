﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject Bala;

    void Start()
    {
        Invoke("Disparar", 1f);
    }

    void Disparar()
    {
        GameObject Player = GameObject.Find ("Avion");
        if (Player != null)
        {
            GameObject Bullet = (GameObject)Instantiate(Bala);
            Bullet.transform.position = transform.position;
            Vector2 direction = Player.transform.position - Bullet.transform.position;
            Bullet.GetComponent<DisparoE>().SetDirection(direction);
        }
    }
}
