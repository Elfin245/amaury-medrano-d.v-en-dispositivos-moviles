﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput.PlatformSpecific;
using UnityStandardAssets.CrossPlatformInput;

public class Disparo : MonoBehaviour
{
    public GameObject Bala;
    public Transform Spawn;

    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            Fire();
        }
    }
    void Fire()
    {
        Instantiate(Bala, Spawn.position, Spawn.rotation);
    }
}
