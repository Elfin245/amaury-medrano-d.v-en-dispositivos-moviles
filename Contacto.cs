﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contacto : MonoBehaviour
{
    public GameObject explosion;
    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        gameController = gameControllerObject.GetComponent<GameController>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Limite") || other.CompareTag("Enemy")) return;
        {
            Instantiate(explosion, transform.position, transform.rotation);
            if (other.CompareTag("Player"))
            {
                Instantiate(explosion, other.transform.position, other.transform.rotation);
                gameController.GameOver();
            }
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
