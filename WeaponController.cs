﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject shot;
    public Transform shotSpawn;

    public float delay;
    public float fireRate;

    void Start()
    {
        InvokeRepeating("Fire", delay, fireRate);
    }

    // Update is called once per frame
    void Fire()
    {
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
    }

}
